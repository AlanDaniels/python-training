#!/usr/bin/env python

# An example of a decorator class.
class Profiler:
    def __init__(self, wrapped):
        self.count = 0
        self.wrapped = wrapped

    def __call__(self, *args):
        self.count += 1
        return self.wrapped(*args)

    def __str__(self):
        return 'Profiler for function "{0}".'.format(self.wrapped.__name__)

    def __del__(self):
        print('All done! Function "{0}" was called {1} times.'.format(self.wrapped.__name__, self.count))

@Profiler
def whatever(x):
    return x ** 2

for x in range(1, 10001):
    print(whatever(x))

print(whatever)
