#!/usr/bin/env python

import random, timeit

# Make a SET of one million random integers:
big_set = set(random.randint(0, 1_000_000_000) for x in range(1_000_000))
set_speed = timeit.timeit('100 in big_set', number=1000, globals=globals())
print('{:.10f}'.format(set_speed))

# Make a list of one million random integers:
big_list = [random.randint(0, 1_000_000_000) for x in range(1_000_000)]
list_speed = timeit.timeit('100 in big_list', number=1000, globals=globals())
print('{:.10f}'.format(list_speed))
