#!/usr/bin/env python

# A "with" statement can protect multiple context managers.
# Note that we have to end with the "\", since we can't wrap all of this in parentheses:
with open('input.txt',  'rt', encoding='utf-8') as fin,\
     open('output.txt', 'wt', encoding='utf-8') as fout:
    for line in fin.readlines():
        if not line.startswith("#"):
            fout.writelines([line])
