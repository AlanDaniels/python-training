#!/usr/bin/env python

# Remember that strings are NOT just a series of bytes.
# If you ever want to write bytes to a file, you need to ENCODE
# those bytes to a particular ENCODING. Unless you're doing
# unusual international characters, just go with UTF-8.

cafe_str = 'caf\xE9'
print(cafe_str)
print(len(cafe_str))

cafe_as_utf8 = cafe_str.encode('utf-8')
print(cafe_as_utf8)
print(len(cafe_as_utf8))

# This will throw an error, since the accented "e" can't be mapped to ascii.
# But, try chaning that "strict" (the default) to "ignore"...
cafe_as_ascii = cafe_str.encode('ascii', errors='strict')
print(cafe_as_ascii)
print(len(cafe_as_ascii))
