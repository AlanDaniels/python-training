# Python Training

Slides and sample programs for LexisNexis Python training, Oct 16th and 17th, 2018.

Here's the itinerary...

Day 1:
* Philosophy
* Basic Types
    * Booleans
    * Integers
    * Strings
    * Floating Point numbers
    * Tuples
    * Lists
    * Dictionaries
    * Sets
    * Other infrequent types
    * The concept of "truthy-ness"
* Basic flow control
    * The "if", "elif", and "else" statements
    * The "for" loop
    * The "while" loop
    * Exception handling
* Useful built-in functions and classes
    * Exceptions
* Creating your own functions
    * The basics
    * Doc-strings
    * Default arguments
    * Keyword arguments
* Creating your own types
    * When *not* to create your own
    * How class inheritance works
    * How class methods work
    * Using "namedtuple"
    * Deriving from "UserDict", "UserList", and "UserString"
    * Overriding "dunder" methods
* Modules
    * Importing other modules
    * What gets imported, exactly?
    * Creating your own module
* Creating your own modules
* Creating your own packages

Day 2:
* Advanced language features
    * Strings are not just a series of bytes
    * Decorators
    * Generators
    * The "with" statement and context managers
* Useful included modules
    * collections
    * sys
    * os.path
    * Handy strings and templates with "string"
    * functools
    * Parsing command line arguments with "argparse"
    * math
    * weakref
* Reading data
    * Crunching through text using regular expressions and "re"
    * Reading and writing CSV files with "csv"
    * Reading and writing JSon files with "json"
    * Creating temporary file names with "tempfile"
* Profiling and debugging
    * Profiling with "profile" and "pstats"
    * Disassemble code with "dis"