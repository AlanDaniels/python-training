#!/usr/bin/env

import os
from os.path import getsize, join
from collections import namedtuple


UsageStats = namedtuple('Usage', ['path', 'disk_space'])

usage_stats = []

for (dir_path, dir_names, file_names) in os.walk('C:\\Temp'):
    if not dir_names:
        disk_space = sum(getsize(join(dir_path, name)) for name in file_names)
        if disk_space > 0:
            usage_stats.append(UsageStats(dir_path, disk_space))

usage_stats.sort(key=lambda x: x.disk_space)
usage_stats.reverse()

for worst in usage_stats[:10]:
    print(worst)
