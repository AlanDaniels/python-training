#!/usr/bin/env python

from collections import defaultdict

nicknames = defaultdict(list)

nicknames['Superman'].append('Kal-El')
nicknames['Superman'].append('The Man Of Steel')
nicknames['Superman'].append('The Last Son Of Krypton')

nicknames['Batman'].append('The Dark Knight')
nicknames['Batman'].append('The Caped Crusader')
nicknames['Batman'].append('The World\'s Greatest Detective')

nicknames['The Flash'].append('The Fastest Man Alive')
nicknames['The Flash'].append('The Scarlet Speedster')

def strip_the(name):
    if name.lower().startswith('the '):
        return name[4:]
    else:
        return name

for key in sorted(nicknames, key=strip_the):
    for alias in nicknames[key]:
        print('{0}, also known as "{1}"'.format(key, alias))
