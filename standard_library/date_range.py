#!/usr/bin/env python

# From the Python Cookbook: https://github.com/dabeaz/python-cookbook

from datetime import datetime, date, timedelta
import calendar

def date_range(start, stop, step):
    while start < stop:
        yield start
        start += step

print('Show a date range in one day increments.')
for d in date_range(date(2018, 10, 1), date(2018, 10, 15), timedelta(days=1)):
    print(d)
print()

print('Show a date range in 30 minute increments.')
for d in date_range(datetime(2018, 10, 1), datetime(2018, 10, 5), timedelta(minutes=30)):
    print(d)
print()
