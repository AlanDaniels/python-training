#!/usr/bin/env python

# The "sys" module is full of useful stuff about the Python interpreter itself.
import sys

print('Where is Python installed?')
print(sys.executable)
print()

print('Where does Python look for its modules?')
for item in sys.path:
    print('   ', item)
print()

print('We are running on platform "{}"'.format(sys.platform))
print()

print('All the flags you can pass to Python when you start it up:')
print(sys.flags)
print()

print('The standard "in", "out", and "error" streams for reading from the command line.')
print(sys.stdin)
print(sys.stdout)
print(sys.stderr)
