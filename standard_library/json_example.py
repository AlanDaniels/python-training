#!/usr/bin/env python

import json

van_halen = '''\
{
    "year": 2018,
    "band": "Van Halen", 
    "members": {
        "lead singer": "Sammy Hagar", 
        "lead guitarist": "Eddie Van Halen",
        "bass guitarist": "Michael Anthony",
        "drummer": "Alex Van Halen"
    }
}'''

# Let us fix the mistakes of the past, and look once again to the future.
data = json.loads(van_halen)
if data['year'] > 2007:
    data['members']['lead singer'] = 'David Lee Roth'
if data['year'] != 1984:
    data['synthesizers'] = False

print(json.dumps(data, indent=4))
