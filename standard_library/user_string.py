#!/usr/bin/env python

from collections import UserString


class UpperCaseString(UserString):
    '''
    A string class that automatically cleans up its initial value,
    and anything appended to it, to be upper-case with no trailing white-space.
    '''

    def __init__(self, value):
        value = value.upper().strip()
        super().__init__(value)

    def __add__(self, new_value):
        new_value = ' ' + str(new_value).upper().strip()
        self.data += new_value
        return self


x = UpperCaseString(' this is uppercase. ')
x += '   everyting i append is cleaned up:  '
x += True
print('"' + x + '"')
