#!/usr/bin/env python

import sqlite3

create_table_SQL = '''\
CREATE TABLE ONE_NAME_STARS (
    ID INT PRIMARY KEY  NOT NULL,
    NAME      TEXT NOT NULL,
    AGE       INT  NOT NULL,
    ADDRESS   CHAR(50),
    NET_WORTH REAL);
'''

insert_prefix = '''\
INSERT INTO ONE_NAME_STARS (ID, NAME, AGE, ADDRESS, NET_WORTH) VALUES 
'''

with sqlite3.connect('stars.db') as conn:
    print('Opened database successfully.')

    conn.execute(create_table_SQL)
    print('Table created successfully.')

    conn.execute(insert_prefix + "(1, 'Adele', 30, 'London',      20000000.00)")
    conn.execute(insert_prefix + "(2, 'Bono',  58, 'Dublin',      35000000.00)")
    conn.execute(insert_prefix + "(3, 'Sting', 67, 'Wallsend',    45000000.00)")
    conn.execute(insert_prefix + "(4, 'Cher',  99, 'Los Angeles', 65000000.00)")
    conn.execute(insert_prefix + "(5, 'Drake', 31, 'Toronto',     27000000.00)")
    conn.execute(insert_prefix + "(6, 'Enya',  57, 'Gweedore',    44000000.00)")
    conn.commit()
    print('Records created successfully.')
