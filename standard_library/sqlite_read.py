#!/usr/bin/env python

import sqlite3


select_SQL = '''\
SELECT ID, NAME, AGE, ADDRESS, NET_WORTH
FROM ONE_NAME_STARS
ORDER BY NET_WORTH DESC
'''

with sqlite3.connect('stars.db') as conn:
    print('Opened database successfully')

    cursor = conn.execute(select_SQL)
    for row in cursor:
        (ident, name, age, address, net_worth) = row
        print(ident, name, age, address, net_worth)

print('All done!')
