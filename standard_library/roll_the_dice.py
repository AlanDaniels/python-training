#!/usr/bin/env python

import random


def roll_the_dice():
    while True:
        dice_1 = random.randint(1, 6)
        dice_2 = random.randint(2, 6)
        total  = dice_1 + dice_2
        if total != 7:
            yield 'Rolled a {}, ({} and {})'.format(total, dice_1, dice_2)
        else:
            return

gambler = roll_the_dice()
for msg in gambler:
    print(msg)
    
print('Finally rolled a seven!')
