#!/usr/bin/env python

with open('builtins.txt', 'wt') as fp:
    for item in dir(__builtins__):
        fp.write(item + '\n')
