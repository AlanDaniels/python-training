#!/usr/bin/env python

def pointless():
    '''This function isn't very useful'''
    pass

pointless()

# Simple positional arguments.
def announce1(first_name, last_name):
    '''First attempt.'''
    print('Hi, my name is {} {}'.format(first_name, last_name)) 

announce1('Jean', 'Grey')

# Default values.
def announce2(first_name, last_name=''):
    '''Second attempt.'''
    print('Hi, my name is {} {}'.format(first_name, last_name))

announce2('Nightcrawler')

# A list of parameters.
def announce3(first_name, last_name='', *aliases):
    '''Third attempt.'''
    print('Hi, my name is {} {}'.format(first_name, last_name))
    for alias in aliases:
        print('    Also known as {}'.format(alias))

announce3('Charles', 'Xavier', 'Professor X', 'Master of the Mind')

# Keywords as parameters.
def announce4(first_name, last_name, *aliases, **kw):
    '''Fourth attempt'''
    print('Hi, my name is {} {}'.format(first_name, last_name))
    for alias in aliases:
        print('    Also known as {}'.format(alias))
    if 'title' in kw:
        print('    Official title:', kw['title'])

announce4('Charles', 'Xavier', 'Professor X', title='Founder')

# Lambdas.
shorthand = lambda nm: 'Hi my name is {}'.format(nm)

print(shorthand('Mystique'))
