#!/usr/bin/env python

# Simple arithmetic operators...

print('7 plus 3')
print(7 + 3)
print()

print('7 minus 3')
print(7 - 3)
print()

print('7 divided by 3, floating point version')
print(7 / 3)
print()

print('7 divided by 3, integer version')
print(7 // 3)
print()

print('The remainder of 8 divided by 3')
print(7 % 3)
print()

print('3 to the 10th power')
print(3 ** 10)
print()

print('3 to the 1000th power')
print(3 ** 1000)
print()

# Simple comparisons...

print('Is 7 equal to 3?')
print(7 == 3)
print()

print('Is 7 NOT equal to 3?')
print(7 != 3)
print()

print('Is 7 less than 3?')
print(7 < 3)
print()

print('Is 7 less than, or equal to, 3?')
print(7 <= 3)
print()

print('Is 7 greater than 3?')
print(7 > 3)
print()

print('Is 7 greater than, or equal to 3?')
print(7 >= 3)
print()
