#!/usr/bin/env python

# The string "format" method is important enough to cover on its own.
# You can think of the "format" syntax to be a "mini-language" inside of Python.

# Use curly brackets to place where you want formatted data to go.
print('Today is {}'.format('Tuesday'))
print()

# Use numbers if you want, starting at zero...
print('Hi, my name is {0} {1}.'.format('Slim', 'Shady'))

# ...otherwise the formatter implicitly counts for you.
print('Hi, my name is {} {}.'.format('Slim', 'Shady'))
print()

# The formatter takes options, followed by a colon (:).

# Use "<" with a length, to pad to the left.
print('---{0:<40}---'.format('Off to the left!'))
print()

# Use ">" with a length, to pad to the right.
print('---{0:>40}---'.format('Off to the right!'))
print()

# Use "^" with a length, to pad in the center.
print('---{0:^40}---'.format('In the center!'))
print()

# You can even proceed the ">", "<" or "^" with a fill-character.
print('---{0:.^40}---'.format('In the center, with dots!'))
print()

# Integers have options too, like using a comma for readability.
print('{0:,}'.format(3 ** 50))
print()

# Use "b" for to print an integer in binary format.
print('{0:b}'.format(12345678))
print()

# Use "x" for hexadecimal format, lower-case.
print('{0:x}'.format(12345678))
print()

# Or, "X" for hexadecimal format, upper-case.
print('{0:X}'.format(12345678))
print()

# Floating point has options, too. Like "e" for exponential notation.
print('{0:e}'.format(3.1 ** 20))
print()

# Or "f" for fixed-point notation.
print('{0:f}'.format(3.1 ** 20))
print()

# You can use a dot to choose how many digits of precision to show.
print('{0:.15f}'.format(3.1 ** 20))
print()

# Use "g" for general format. The formatter chooses the right way to go.
# It shows as fixed-point if small, or exponential if large.
print('{0:g}'.format(3.1 ** 5))
print('{0:g}'.format(3.1 ** 20))
print()
