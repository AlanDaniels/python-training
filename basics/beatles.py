#!/usr/bin/env python

# Let's write a function to determine if a given name is a Beatle.
# We'll start with a poor implementation, and work towards a better one.
# Remember: Less code, more data!


# Round 1: How somebody fresh from Java would write it...
def is_beatle_1(first_name, last_name):
    if first_name == 'John' and last_name == 'Lennon':
        return True
    elif first_name == 'Paul' and last_name == 'McCartney':
        return True
    elif first_name == 'George' and last_name == 'Harrison':
        return True
    elif first_name == 'Ringo' and last_name == 'Starr':
        return True
    else:
        return False

print(is_beatle_1('John', 'Lennon'))
print(is_beatle_1('Pete', 'Best'))
print()


# Round 2: But, remember that we can use compare Tuples!
def is_beatle_2(first_name, last_name):
    if (first_name, last_name) == ('John', 'Lennon'):
        return True
    elif (first_name, last_name) == ('Paul', 'McCartney'):
        return True
    elif (first_name, last_name) == ('George', 'Harrison'):
        return True
    elif (first_name, last_name) == ('Ringo', 'Starr'):
        return True
    else:
        return False

print(is_beatle_2('Paul', 'McCartney'))
print(is_beatle_2('Pete', 'Best'))
print()


# Round 3: Wait a second, we're just seeing if some value is
# in a collection, right? Well then, we should be using sets.
def is_beatle_3(first_name, last_name):
    beatles = {('John', 'Lennon'),
               ('Paul', 'McCartney'),
               ('George', 'Harrison'),
               ('Ringo', 'Starr')}
    return (first_name, last_name) in beatles

print(is_beatle_2('Ringo', 'Starr'))
print(is_beatle_2('Pete', 'Best'))
print()
