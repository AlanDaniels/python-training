#!/usr/bin/env python

# Note, a list of dictionaries.
portfolio = [
    {'name': 'IBM',  'shares': 100, 'price':  90.10},
    {'name': 'APPL', 'shares':  50, 'price': 543.22},
    {'name': 'FB',   'shares': 200, 'price':  21.09},
    {'name': 'HPQ',  'shares':  35, 'price':  31.75},
    {'name': 'YHOO', 'shares':  45, 'price':  16.35},
    {'name': 'ACME', 'shares':  75, 'price': 115.65},
]

for entry in portfolio:
    if entry['name'] in ['HPQ', 'YHOO']:
        entry['sell'] = True

total = 0.0
for entry in portfolio:
    should_sell = entry.get('sell', False)
    value = entry['shares'] * entry['price']
    if should_sell:
        print('{0:4} => ${1:,.2f} (Sell later)'.format(entry['name'], value))
    else:
        print('{0:4} => ${1:,.2f}'.format(entry['name'], value))
    total += value

print('Total value: ${:,.2f}'.format(total))
