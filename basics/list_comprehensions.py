suits = ['Hearts', 'Spades', 'Diamonds', 'Clubs']
ranks = ['Ace', 'King', 'Queen', 'Jack'] + list(range(1, 10))

cards = ['{} of {}'.format(rank, suit)
         for suit in suits
         for rank in ranks]

for card in cards:
    print(card)
