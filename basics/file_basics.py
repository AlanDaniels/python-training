#!/usr/bin/env python

heroes = ['Superman', 'Batman', 'Wonder Woman',
          'The Flash', 'Aquaman', 'Cyborg']

# Even if we had an exception, the file would still close.
with open('heroes.txt', mode='wt', encoding='utf-8') as fp:
    for hero in heroes:
        fp.writelines([hero + '\n'])
