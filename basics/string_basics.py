#!/usr/bin/env python

print('Strings can be wrapped in single quotes')
print()

print("Strings can be wrapped in double quotes, too.")
print()

print('''Strings can be triple-quoted,
which means they can span multiple lines. ''')
print()

print("""Strings can be triple-quoted,
either with single quotes or double quotes. """)
print()

print('You can also add a new line, explicitly,\nby using a slash-n.')
print()

print('Tabs are done \t\t\t with a slash-t.')
print()

print('We can do Unicode mark-up with slash-x and a number: caf\xE9')
print()
