#!/usr/bin/env python

avengers = ['Captain America', 'Iron Man', 'Hulk', 'Captain America']
print(avengers)
print()

print('Is this right?', 'Black Widow' in avengers)
print()

avengers.append('Black Widow')
print('Now it is!', avengers)
print()

print('How many Hawkeyes?', avengers.count('Hawkeye'))
print()

avengers.append('Hawkeye')
print('We fixed that too.', avengers)
print()

print('Who is the first Avenger?', avengers[0])
print()

print('Who is the last Avenger?', avengers[-1])
print()
