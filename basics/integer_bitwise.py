#!/usr/bin/env python

# There are "bit-wise" integer operators, which in practice, you won't run into very often.
# You only need these if you're doing low-level bit fiddling. I'm just listing them here
# for the sake of being thorough.

# Bitwise "and"-ing of two integers, using "&".
print(2 & 3)
print()

# Bitwise "or"-ing of two integers, using "|".
print(2 | 3)
print()

# Bitwise "exclusive-or"-ing of two integers, using "^".
print(2 ^ 3)
print()

# Inverting the bits of a number.
print(~666)
print()

# Bit-shifting upwards, using "<<".
print(1 << 5)
print()

# Bit-shifting downwards, using ">>".
print(64 >> 5)
print()
