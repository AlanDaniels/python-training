#!/usr/bin/env python

class LogWriter:
    
    # We'll initialize with a file name.
    def __init__(self, filename):
        self.filename = filename

    # When we enter the "with" statement, Python calls the "enter" dunder method.
    # Note that we need to return "self" here.
    def __enter__(self):
        self.fp = open(self.filename, 'wt', encoding='utf-8')
        header = 'Today\'s log file...\n'
        self.fp.writelines([header])
        return self

    # And, python calls the "exit" dunder method on the way out, no matter what happens.
    # Even if an error happens inside the "with" statement, the file still gets closed.
    def __exit__(self, type, value, traceback):
        footer = 'And that\'s everything for today.\n'
        self.fp.writelines([footer])
        self.fp.close()
        if any([type, value, traceback]):
            print(type, value, traceback)


# Let's try it out.
with LogWriter('test.txt') as log:
    for i in range(1, 21):
        line = 'Entry #{}...\n'.format(i)
        log.fp.writelines([line])
        if i == 11:
            raise ValueError('Boom!')
