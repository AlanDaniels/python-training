#!/usr/bin/env python

class OscarWinner:
    def __init__(self, first_name, last_name, movie):
        self.first_name = first_name
        self.last_name = last_name
        self.movie = movie

    def movie_trivia(self):
        return '{} {} won an Oscoar for the file "{}"'.format(
            self.first_name, self.last_name, self.movie)


class EmmyWinner:
    def __init__(self, first_name, last_name, album):
        self.first_name = first_name
        self.last_name = last_name
        self.album = album

    def album_trivia(self):
        return '{} {} won an Emmy for the album "{}"'.format(
            self.first_name, self.last_name, self.album)


class MultipleAwardWinner(OscarWinner, EmmyWinner):
    def __init__(self, first_name, last_name, movie, album):
        OscarWinner.__init__(self, first_name, last_name, movie)
        EmmyWinner.__init__(self, first_name, last_name, album)


mel_brooks = MultipleAwardWinner('Mel', 'Brooks', 
                 movie='The Producers', album='The 2000 Year Old Man in the Year 2000')
print(mel_brooks.movie_trivia())
print(mel_brooks.album_trivia())
