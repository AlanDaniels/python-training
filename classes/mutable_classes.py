#!/usr/bin/env python

# Classes are naturally considered "immutable",
# Which means you can use them as keys in dictionaries, add them to sets, etc.
class ImmutableClass:
    '''I don't have to do a thing here.'''
    pass

solid = ImmutableClass()
print(hash(solid))
solid_set = {solid}
print(solid_set)


# So, how do I warn everybody that I change over time? Define "__hash__" as None.
class ChangingClass:
    __hash__ = None

fluid = ChangingClass()
print(hash(fluid))
