#!/usr/bin/env python

# A class can override the "__call__" method to pretend to be function.
class Counter:
    def __init__(self, initial_value=0):
        self.value = initial_value

    def __call__(self, added=1):
        self.value += added
        return self.value


# Every time we call this thing, it increases.
# Note that it looks just like a function.
c = Counter(10)
print(c())
print(c(2))
print(c(3))
print(c())
