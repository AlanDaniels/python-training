#!/usr/bin/env python

class Actor:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class OscarWinner(Actor):
    def __init__(self, first_name, last_name, movie):
        super().__init__(first_name, last_name)
        self.movie = movie

    def movie_trivia(self):
        return '{} {} won an Oscoar for the file "{}"'.format(
            self.first_name, self.last_name, self.movie)


francis = OscarWinner('Francis', 'McDormand', 'Three Billboards Outside Ebbing, Missouri')
print(francis.movie_trivia())
