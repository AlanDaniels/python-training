#!/usr/bin/env python

import math


def log_activity(func):
    def wrapped(*args, **kwargs):
        print('Calling {}...'.format(func.__name__))
        result = func(*args, **kwargs)
        print('...result of {} was{}'.format(func.__name__, result))
        return result
    return wrapped


@log_activity
def get_sin_and_cosin(val):
    return (math.sin(val), math.cos(val))


x = get_sin_and_cosin(math.pi / 3.0)
print(x)
