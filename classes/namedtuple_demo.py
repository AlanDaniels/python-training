#!/usr/bin/env python

from collections import namedtuple

Actor = namedtuple('Actor', ['first_name', 'last_name', 'character', 'movie', 'year'])

doc_brown = Actor('Christopher', 'Lloyd', 'Doc Brown', 'BTTF', 1985)
murphy    = Actor('Peter', 'Weller', 'Murphy', 'Robocop', 1987)
t1000     = Actor('Robert', 'Patric', 'T-1000', 'Terminator 2', 1991)

print(doc_brown)
print(murphy) 
print(t1000)

print(doc_brown[0])
print(doc_brown.first_name)
