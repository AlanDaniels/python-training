#!/usr/bin/env python

class Actor:

    # Almost every class will have an "init" dunder method.
    def __init__(self, first_name, middle_name, last_name):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name

    # The "str" dunder method is called by Python when you try to look at an
    # object as if it were a string, typically via "print" or "str".
    def __str__(self):
        return '{}, {} {}'.format(self.last_name, self.first_name, self.middle_name)


marty = Actor('Michael', 'J.', 'Fox')
print(marty)
