#!/usr/bin/env python

class SomeClass:
    # Normal methods get the instance as the first parameter, traditionally called "self".
    def hello(self, x):
        print('Hello from a normal method', self, x)

    # A "class method" will pass in the class itself, instead.
    @classmethod
    def class_hello(cls, x):
        print('Hello from a class method', cls, x)

    # A "static method" won't pass in anything at all.
    @staticmethod
    def static_hello(x):
        print('Hello from a static method', x)


thing = SomeClass()
thing.hello('Hi')
thing.class_hello('Howdy')
thing.static_hello('Yo')
