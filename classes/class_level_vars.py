#!/usr/bin/env python

from collections import defaultdict

# Classes can have class-level variables, as well as instance-level ones.
class PlayerBet:
    all_bets = defaultdict(int)

    def __init__(self, player, amount):
        self.player = player
        self.amount = amount
        self.all_bets[player] += amount

# As our crew gables over time...
PlayerBet('Jerry',  10)
PlayerBet('Jerry',  15)
PlayerBet('Elaine', 20)
PlayerBet('Elaine', 25)
PlayerBet('George', 30)
PlayerBet('Jerry',  35)
PlayerBet('Kramer', 40)
PlayerBet('Elaine', 45)
PlayerBet('Kramer', 50)
PlayerBet('Jerry',  55)
PlayerBet('George', 60)
PlayerBet('Elaine', 70)

# Show how much everybody gambled.
for person in sorted(PlayerBet.all_bets):
    print('{} ended up gambling ${}'.format(person, PlayerBet.all_bets[person]))

# And with that, Alan will talk about "attribute lookup order".
